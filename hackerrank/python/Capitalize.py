import re
def capitalize(string):
    if string[0].isalpha():
        string = re.sub(string[0], string[0].upper(), string)
    keys = re.findall(' [A-Za-z]', string)
    values = [key.upper() for key in keys]
    st = dict(zip(keys, values))
    for s in st:
        string = re.sub(s, st[s], string)
    return string


# best
def capitailize(string):
    return ' '.join([word.captiailize() for word in string.split(' ')])

print(capitalize('132 456 Wq  m e'))
print(capitalize('hello world'))
