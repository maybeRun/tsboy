class Solution(object):
    def missingNumber(self, nums):
        """
        :type nums: List[int]
        :rtype: int
        """
        right_nums = set(range(1, nums[-1]))
        nums = set(nums)
        if list(right_nums - nums):
            return list(right_nums - nums)[0]

s = Solution()
print(s.missingNumber([0]))


# best
class Solution(object):
    def missingNumber(self, nums):
        """
        :type nums: List[int]
        :rtype: int
        """
        # soluation 1
        '''
        sum = 0
        for item in nums:
            sum += item
        return len(nums)*(len(nums)+1)/2 - sum
        '''
        # soluation 1 - updated
        return len(nums)*(len(nums)+1)/2 - sum(nums)