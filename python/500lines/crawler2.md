## 协程
我们给你一个承诺.他是可能的写异步的代码结合有效率的回调函数用典型的看起来想多线程程序.这个结合实现模式被叫做协程.使用python3.4标准的异步库和一个被叫做aiohttp的包,爬取url通过协程是非常直观的.
```
@asyncio.coroutine
 def fetch(self, url):
     response = yield from self.session.get(url)
     body = yield from response.read()
```
他也是可以拓展的.相对于每个线程消耗50K和操作系统对线程的限制,一个python线程仅仅消耗3k内存在Jesse's的电脑.python很容易启动成百上千的协程.

协程的概念,计算机早期来说,是简单的:他是一个子程序可以被暂停或启动.然而线程是在操作系统中多任务抢占处理,协程的多任务是合作,他们选择什么时候暂停,和哪一个协程取运行接下来.

有很多方法实现这个,甚至在python中都有好几个.python3.4的asyncio库实现的协程,被构建在生成器上,Funture类,和yield from语句.在python3.5协程是一个语言的原生特质,然而,理解python3.4使用语言基本的库实现的协程,是理解python3.5原生协程的基础.

为了解释python3.4基于生成器的协程,我们需要理解生成器和如何被使用,相信你会享受读就像我享受写一样.我们解释了基于协程的异步,我们就会使用在爬虫.

## python协程是如何工作的
在你把握python生成器之前，你需要理解，普通的python函数是如何工作的。通常，通常当一个python进程调用一个子进程的时候，这个进程保留控制知道返回或者抛异常。然后控制才返回给调用者：
```
def  foo():
    bar()
def bar():
    pass
```
标准的python解释其使用C写的。C的方法执行python被调用的方法。他需要一个python的堆栈框架对象并且执行python字节码在内容框架上。这里是foo字节码：
```
import dis
dis.dis(foo)
2           0 LOAD_GLOBAL              0 (bar)
             3 CALL_FUNCTION            0 (0 positional, 0 keyword pair)
             6 POP_TOP
             7 LOAD_CONST               0 (None)
            10 RETURN_VALUE
```
foo调用bar方法到他的堆栈，然后扔出从栈顶，加载None栈顶，最后返回None

当PyEval_EvalFrameEx遇上CALL_FUNCTION字节码的时候，创造一个新的python堆栈和递归：也就是说用新的递归调用PyEval_EvalFrameEx去执行bar。

理解python的堆内存的栈空间的分配！python的解释器一般是C写的，所以它的堆栈是正常的堆栈。但是他是在栈上操作堆。令人吃惊的是，这就意味着，python的堆栈可以存活超过框架的调用。可以交互式的看到，保存当前bar的内存。
```
>>> import inspect
>>> frame = None
>>> def foo():
...     bar()
...
>>> def bar():
...     global frame
...     frame = inspect.currentframe()
...
>>> foo()
>>> # The frame was executing the code for 'bar'.
>>> frame.f_code.co_name
'bar'
>>> # Its back pointer refers to the frame for 'foo'.
>>> caller_frame = frame.f_back
>>> caller_frame.f_code.co_name
'foo'
```
这里有相同的设定在python的生成器，有令人着迷的作用
```
>>> def gen_fn():
...     result = yield 1
...     print('result of yield: {}'.format(result))
...     result2 = yield 2
...     print('result of 2nd yield: {}'.format(result2))
...     return 'done'
...     
```
当python去编译gen_fn看到yield就知道这是一个生成器方法，不是普通的方法。他会设置一个标记来记住这个事实：
```
>>> # The generator flag is bit position 5.
>>> generator_bit = 1 << 5
>>> bool(gen_fn.__code__.co_flags & generator_bit)
True
```
当你调用一个生成器方法，python看到一个生成器的标志，并不会真正去运行程序，相反的，他会创建生成器。
```
>>> gen = gen_fn()
>>> type(gen)
<class 'generator'>
```
python的生成器封装了一写堆栈和一些代码的引用。
```
>>> gen.gi_code.co_name
'gen_fn'
```
来自于gen_fn方法的所有的生成器都指向相同的代码。但是每一个都有自己的堆栈。这些堆栈帧不再任何堆栈上，在堆内存等待被使用：

这个框架有最后一个指令的指针，他是近期执行的最后一个指令。在刚开始，最后的指令指向-1,意味着生成器没有开始：
```
>>> gen.gi_frame.f_lasti
-1
```
当你调用send的时候，生成器返回第一个yield，暂停。返回的值是1：
```
>>> gen.send(None)
1
```
生成器指针从开始的三个字节码，到现在的56个字节码：
```
>>> gen.gi_frame.f_lasti
3
>>> len(gen.gi_code.co_code)
56
```
生成器可以被恢复，因为他的存储地址不是在栈上，而是在堆上。它的位置不是固定的，他不需要遵循先进先出这种一半程序的规则。他想云一样灵活。

我们可以发送hello给生成器，它变成yield的结果，生成器继续知道返回2
```
>>> gen.send('hello')
result of yield: hello
2
```
内存包含了result变量
```
>>> gen.gi_frame.f_locals
{'result': 'hello'}
```
其他的生成器被创建将有自己的内存和自己的变量

当我们再一次调用send，生成器继续执行，最后抛出异常：
```
>>> gen.send('goodbye')
result of 2nd yield: goodbye
Traceback (most recent call last):
  File "<input>", line 1, in <module>
StopIteration: done
```
这个异常有一个值，返回生成器的值done

# 构建协程通过生成器
由于生成器可以暂停,并且可以恢复,也返回值.听起来是一个很好的原型来构建一个异步程序模型,不需要恶心的callbacks!我们想去构建一个协程:一个子进程合作调度于其他的子进程在程序里.我们的协程是一个简单的版本通过使用python标准库的asyncio库.我们用生成器,futures和yield from实现asyncio.

首先,我们需要一个方法去表示协程正在等待一些未来的结果.一个简单的版本:
```
class Future:
    def __init__(self):
        self.result = None
        self._callbacks = []

    def add_done_callback(self, fn):
        self._callbacks.append(fn)

    def set_result(self, result):
        self.result = result
        for fn in self._callbacks:
            fn(self)
```

让我们使用futures和协程来写我们的fetcher.我们写fetch带着回调.
```
class Fetcher:
    def fetch(self):
        self.sock = socket.socket()
        self.sock.setblocking(False)
        try:
            self.sock.connect(('xkcd.com', 80))
        except BlockingIOError:
            pass
        selector.register(self.sock.fileno(),
                          EVENT_WRITE,
                          self.connected)

    def connected(self, key, mask):
        print('connected!')
        # And so on....
```
fetche方法开始连接一个socket,然后注册一个callback函数,connected,当socket准备好的时候开始执行.现在我们可以组合两部到一个协程.
```
def fetch(self):
        sock = socket.socket()
        sock.setblocking(False)
        try:
            sock.connect(('xkcd.com', 80))
        except BlockingIOError:
            pass

        f = Future()

        def on_connected():
            f.set_result(None)

        selector.register(sock.fileno(),
                          EVENT_WRITE,
                          on_connected)
        yield f
        selector.unregister(sock.fileno())
        print('connected!')
```
现在fetch是一个生成器方法,而不是一个普通的方法,因为它包含了一个yield语句.我们调用了future,然后yield它,暂停fetch直到socket连接准备好的时候.

但是当future执行的时候,生成器怎么恢复?我们需要一个协成来去董.我们叫它task
```
class Task:
    def __init__(self, coro):
        self.coro = coro
        f = Future()
        f.set_result(None)
        self.step(f)

    def step(self, future):
        try:
            next_future = self.coro.send(future.result)
        except StopIteration:
            return

        next_future.add_done_callback(self.step)

# Begin fetching http://xkcd.com/353/
fetcher = Fetcher('/353/')
Task(fetcher.fetch())

loop()
```
