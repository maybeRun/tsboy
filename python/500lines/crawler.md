source:https://github.com/aosabook/500lines/blob/master/crawler/crawler.markdown
pubtime:2016-7

# 一个用asyncio Coroutines的网络爬虫
作者:A. Jesse Jiryu Davis and Guido van Rossum
Jesse Jiryu Davis,这个人是一个工程师在MongoDB在纽约.他写了Motor,异步的MongoDB的python驱动,他是MongoDB C模块的领导和PyMongo的成员.他也贡献代码在asyncion和Tornado.他的博客:[http://emptysqua.re.](http://emptysqua.re.)

Guido van Rossum是python之父,python是网络上一个主要的语言,python社区称呼他为仁慈的独裁者.他的网站是[http://www.python.org/~guido/.](http://www.python.org/~guido/.)

## 简介
传统的计算机科学强调有效率的算法用来计算机计算的尽可能快.但是很多网络程序花费时间不再计算,而是连接慢,或者是罕见的实践.这些程序有一个不同的挑战,去等待大量的网络的事件.这个问题现在的解决方式是asynchronous I/O或者async.

这个章节呈现一个简单的web爬虫.爬虫是一个在原型上异步的程序,因为他要等待很多响应,但是很少cpu计算.一次能够爬去越多的页面,就越快完成.如果每一个请求用一个线程,随着请求的增加,回内存溢出或者溢出其他与线程相关的资源.使用asynchronous I/O可以避免这中问题.

我们展示在三个阶段.首先,我们展示异步事件循环.绘制一个使用时间循环和回调函数的爬虫数据网络.他是非常有效率的,但是拓展他将是一个很大的问题代码会变成一坨屎一样.第二个阶段,我们展示python携程的高效和可拓展,我们实现简单的携程通过使用python的generator方法.第三个阶段,我们使用最有前景的携程从python标准库的asyncio,并且调度他们用async队列.

## 任务
一个网络爬虫发现和下载所有的页面,可能取存储或者索引他们.开始用一个根路径url,她爬取每一个页面,解析url对于没出现过得页面,添加到队列.它停下来在爬下来的页面没有出现之前没出现过得url,或者队列是空的时候.

我们可以加速这个过程通过同时下载多个页面.当发现新的连接的时候,启动相似的fetcher操作.爬虫解析response,并且把连接放进队列.太多的并发可能反而导致性能降低,所以我们控制请求的并发数,留下一些链接在队列中,知道规定的请求完成的时候在进行请求.

## 传统(low逼)的方法
我们怎么同时爬取?传统上我们创建线程池.每一个线程负责下载一个页面.例如下载一个页面xkcd.com
```
def fetch(url):
    sock = socket.socket()
    sock.connect(('xkcd.com', 80))
    request = 'GET {} HTTP/1.0\r\nHost: xkcd.com\r\n\r\n'.format(url)
    sock.send(request.encode('ascii'))
    response = b''
    chunk = sock.recv(4096)
    while chunk:
        response += chunk
        chunk = sock.recv(4096)

    # Page is now downloaded.
    links = parse_links(response)
    q.add(links)
```
默认情况下,socket连接是阻塞的:当线程调用connect和recv方法的时候,暂停知道操作完成.所以下载很多页面一次,我们需要很多线程.一个复杂的程序减少线程创建的消耗通过保持一个线程池,然后检查他们并且重用他们,他做相同的socket连接在一个连接池.

但是,线程是昂贵的,操作系统规定了一个程序的线程数量.jesse的电脑里面,python进程消耗50k在内存,开始数万个进程就会失败.如果我们同时进行几万个线程,我们用完线程之前套接字就用完了.线程的开销或者系统线程的限制是瓶颈.

在他有影响里的文章里,Dan Kegel提到了I/O并发的多线程限制,他说
> 现在网络服务可以处理成千上万的请求了,你不觉得吗?

Kegel在1999年创建了C10K这个词.现在几万连接听起来很容易,但是只是改变了大小而不是方式.在当时,使用一个线程处理几万的连接是不切实际的.现在容量已经是当初的好几个数量集了.的却,我的爬虫确实可以工作的很好通过线程,但是对于大规模的应用来说,有成百上千的连接,这个缺陷依然在.大部分的操作系统依然可以创建socket但是不能够创建线程了.我们应该怎么克服?

#Async
Asynchronous I/O框架做并发通过一个单独的线程使用不阻塞的socket连接.在我们的async爬虫中,我们设定socket不阻塞在我们开始连接服务器之前.
```
sock = socket.socket()
sock.setblocking(False)
try:
    sock.connect(('xkcd.com', 80))
except BlockingIOError:
    pass
```

恼人的是,一个非阻塞的socket连接抛出一个异常,甚至当他在普通的运行的时候.这个异常复制了底层c模块的行为,设置errno为EINPROGRESS告诉你当他开始的时候.
现在我们的爬虫需要一个方法知道什么时候爬虫是稳定的,它可以发送http请求.我们可以简单的写一个循环.
```
request = 'GET {} HTTP/1.0\r\nHost: xkcd.com\r\n\r\n'.format(url)
encoded = request.encode('ascii')

while True:
    try:
        sock.send(encoded)
        break  # Done.
    except OSError as e:
        pass

print('sent')
```
这个方法不仅浪费电,而且不能有效的暂停事件多个sockets的时候.之前BSD的解决方案是select,一个等待时间报错的非阻塞的C模块.现在,互联网有大量连接的需求,产生了很多可以替代的产品,比如poll,BSD的方案kqueue和epoll对于linux.这些API是相似的和select,但是表现的更好对于大数量的连接.

pythonh3.4的DefaultSelector看起来是最好的select.注册网络I/O,我们创造一个非阻塞的socket和注册它通过默认的选择器:
```
from selectors import DefaultSelector, EVENT_WRITE

selector = DefaultSelector()

sock = socket.socket()
sock.setblocking(False)
try:
    sock.connect(('xkcd.com', 80))
except BlockingIOError:
    pass

def connected():
    selector.unregister(sock.fileno())
    print('connected!')

selector.register(sock.fileno(), EVENT_WRITE, connected)
```
我们忽视伪错误,调用selector.register,传递socket文件描述符和一个实践等待的变量.当这个连接建立的时候,通过EVENT_WRITE通知我们,我们想知道什么时候socket可写.我们也通过一个python方法,connected,当事件发生错误的时候运行,就像一个方法callback.

我们的程序接受I/O通知,在一个循环通过selector接收
```
def loop():
    while True:
        events = selector.select()
        for event_key, event_mask in events:
            callback = event_key.data
            callback()
```
connected回调函数存储作为event_key.data,一旦socket连接就开始执行和接受.

不像我们之前的哪种循环,select的调用可以暂停,等待下一个I/O.循环运行callback等待事件.还没有完成的操作会保持等待知道未来事件完成.

我们怎么证明,我们展示如何开始一个操作和执行一个callback当这个操作准备好的时候.一个异步的框架构建在我们已经占是的两个方面上,非阻塞的socket和事件循环,运行并发的操作在一个线程.

我们已经实现了并发,但是不想传统所说的并行.我们构建了一个小型的I/O系统.它可以开始新的操作当其他的操作阻塞的时候.他其实实际上并没有利用多个核心来处理并行计算.这个系统被用来处理I/O阻塞的问题而不是CUP阻塞的问题.

所以我们的事件循环是有效的在I/O,因为他没有致力于把每个线程分配给连接.但是在我们继续之前,那是重要的纠正一个误区,async是更快地比多线程.通常他不是,事实上,在python中,一个事件循环向我们这样的通常是慢一点比多线程,当服务于很少的连接,或者一些非常活跃的连接.在没有全局锁的情况下,线程通常执行的更好在这个工作量下.什么样子的asynchronous I/O 是正确的,程序有很多缓慢的不活跃的连接.

## 带着callback的进程
随着我们构建起来async框架,我们如何构建一个web爬虫?甚至一个简单的URL-fetcher都是痛苦的去写.

我们开始从一组我们要爬取的url,和一组我们看到过的url.
```
urls_todo = set(['/'])
seen_urls = set(['/'])
```
seen_urls包含了urls_todo加上已经完成的url.这两组url初始化通过根url/

爬取一个页面需要一些callback,connceted的callback触发当socked被连接的时候,发送一个get请求给服务器.但是之后必须等待一个response,所以它注册了另一些callback,如果当callback调用,依然不能读取所有的response,就会重新注册.

让我们收集callback给Fetcher对象,它需要URL,socket对象,和一个读取response的地方.

```
class Fetcher:
    def __init__(self, url):
        self.response = b''  # Empty array of bytes.
        self.url = url
        self.sock = None
```
我们开始通过调用Fetcher.fetcher:
```
# Method on Fetcher class.
def fetch(self):
    self.sock = socket.socket()
    self.sock.setblocking(False)
    try:
        self.sock.connect(('xkcd.com', 80))
    except BlockingIOError:
        pass

    # Register next callback.
    selector.register(self.sock.fileno(),
                      EVENT_WRITE,
                      self.connected)
```
fetcher方法开始连接socket.但是注意,方法返回在成功连接之前.它必须返回控制事件循环等待连接.想要理解这个,想象我们整个程序的结构是这样的:
```
# Begin fetching http://xkcd.com/353/
fetcher = Fetcher('/353/')
fetcher.fetch()

while True:
    events = selector.select()
    for event_key, event_mask in events:
        callback = event_key.data
        callback(event_key, event_mask)
```
当调用select的时候所有事件循环的通知都会被处理.因此fetcher必须控制事件循环,以至于程序知道socket什么时候被连接.只有这样才能运行connected的callbcak,被注册在fetcher的最后.

这里是connected的实现:
```
# Method on Fetcher class.
def connected(self, key, mask):
    print('connected!')
    selector.unregister(key.fd)
    request = 'GET {} HTTP/1.0\r\nHost: xkcd.com\r\n\r\n'.format(self.url)
    self.sock.send(request.encode('ascii'))

    # Register the next callback.
    selector.register(key.fd,
                      EVENT_READ,
                      self.read_response)
```
这个方法发送一个get请求.一个真正的程序会检查返回值以防整个数据不能被一下在发送过去.但是我们的程序是简单的.轻率取得调用send,然后等待一个response.下一个回调函数,read_response处理服务器回复.
```
# Method on Fetcher class.
   def read_response(self, key, mask):
       global stopped

       chunk = self.sock.recv(4096)  # 4k chunk size.
       if chunk:
           self.response += chunk
       else:
           selector.unregister(key.fd)  # Done reading.
           links = self.parse_links()

           # Python set-logic:
           for link in links.difference(seen_urls):
               urls_todo.add(link)
               Fetcher(link).fetch()  # <- New Fetcher.

           seen_urls.update(links)
           urls_todo.remove(self.url)
           if not urls_todo:
               stopped = True
```
这个进程再每次socket可读的时候执行.这可能意味这两件事,socket可读或者关闭了.

回调函数请求4000k数据从socket,chunk包含有需要的数据.如果多余4000k,chunk是4000k并且socket继续保持可读,所以事件循环继续运行.当响应完成,这个服务关闭socket,chuck是空的.

parse_links方法,没有展示,返回一组URLS.我们开始一个新的fetcher对于每一个新的url,没有并发上限.在async程序一个很明显的特征是带着callbcak:我们不需要对共享数据互斥,例如,当我们添加url到seen_urls的时候.没有抢先的多任务,我们代码不能中断.

我们添加一个全局的stopped变量来控制循环:
```
stopped = False

def loop():
    while not stopped:
        events = selector.select()
        for event_key, event_mask in events:
            callback = event_key.data
            callback()
```

一旦所有的页面被下载完,fetcher停止全局是事件循环并且退出.

这个例子说明了async问题所在:意大利面代码.我们需要一个方法展示一系列计算和I/O操作,并且调度一些这样的操作取并发运行.但是如果没有线程,一些列函数不可能到一个单一的方法中:当一个I/O操作开始的时候,她严格的保存他为来所需要的状态.你要思考写这样保存状态的代码.

让我们解释我们意味着什么.思考多么简单我们爬取URL在一个线程与一个普通的阻塞socket:
```
# Blocking version.
def fetch(url):
    sock = socket.socket()
    sock.connect(('xkcd.com', 80))
    request = 'GET {} HTTP/1.0\r\nHost: xkcd.com\r\n\r\n'.format(url)
    sock.send(request.encode('ascii'))
    response = b''
    chunk = sock.recv(4096)
    while chunk:
        response += chunk
        chunk = sock.recv(4096)

    # Page is now downloaded.
    links = parse_links(response)
    q.add(links)
```
