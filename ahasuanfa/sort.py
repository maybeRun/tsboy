# 桶排序(O(M+N))
"""输入几个数次,按照排序输出

申请一个数组, 按照每个位置的索引数字出现的次数存储
"""


def barrel_sort(nums):
    a = []
    for i in range(10):
        a.append(0)

    for n in nums:
        a[n] = a[n] + 1

    rst = []
    for k, n in enumerate(a):
        for i in range(n):
            rst.append(k)
    print(rst)
    return a


# 冒泡排序, 相邻两个数相互比较, 每次把最小的数放到最后.O(N**2)
def bubble_sort(nums):
    for i in range(len(nums) - 1):
        for n, k in enumerate(nums):
            if n < len(nums) - 1:
                if k < nums[n + 1]:
                    nums[n], nums[n + 1] = nums[n + 1], k
    return nums


# 快速排序, 设置一个基数,找到基数的位置,使得基数左边的数字小于这个基数,基数右边的数字大于这个基数.O(NlogN)
# 版本1
def fast_sorted(nums):
    if len(nums) == 1:
        return nums
    elif len(nums) == 2:
        if nums[0] > nums[1]:
            nums = [nums[1], nums[0]]
        return nums
    else:
        base = nums[0]
        for rn, rk in enumerate(reversed(nums)):
            rn = len(nums) - rn - 1
            if rk < base:
                for ln, lk in enumerate(nums):
                    if ln == rn:
                        nums[0], nums[ln] = nums[ln], nums[0]
                        return fast_sorted(nums[:ln]) + [base] + fast_sorted(nums[ln + 1:])
                    elif ln > 1 and lk > base:
                        nums[ln], nums[rn] = rk, lk
                        break

        else:
            return [nums[0]] + fast_sorted(nums[1:])

    return nums


# 版本2
def fast_sorted2(L):
    if len(L) < 2: return L
    pivot_element = random.choice(L)
    small = [i for i in L if i < pivot_element]
    medium = [i for i in L if i == pivot_element]
    large = [i for i in L if i > pivot_element]
    return qsort(small) + medium + qsort(large)



print(fast_sorted([3, 2, 1, 4, 7, 5, 8, 6, 9, 6]))
