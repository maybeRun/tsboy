import os
import time
import sys
import requests
from concurrent import futures

POP20_CC = ('CN IN US ID BR PK NG BD RU JP MX PH VN ET EG DE IR TR CD FR').split()
BASE_URL = 'http://shixin.court.gov.cn/captchaNew.do?captchaId=dcb9c528d13547d2bfdfcc4464a50b6d&random=0.8784878150636275'
DEST_DIR = 'downloads/'
MAX_WORKERS = 20

def save_flag(img, filename):
    path = os.path.join(DEST_DIR, filename)
    with open(path, 'wb') as fp:
        fp.write(img)


def get_flag():
    url = BASE_URL
    resp = requests.get(url)
    time.sleep(1)
    return resp.content


def show(text):
    print(text, end=' ')
    sys.stdout.flush()

def download_many(cc_list):
    for cc in sorted(cc_list):
        image = get_flag()
        show(cc)
        save_flag(image, cc.lower())
    return len(cc_list)

def download_one(cc):
    image = get_flag()
    show(cc)
    save_flag(image, cc.lower())
    return cc

def download_futures(cc_list):
    workers = min(MAX_WORKERS, len(cc_list))
    with futures.ThreadPoolExecutor(workers) as e:
        res = e.map(download_one, sorted(cc_list))
        return len(list(res))


def download_many_futures(cc_list):
    cc_list = cc_list[:5]
    with futures.ThreadPoolExecutor(max_workers=3) as e:
        to_do = []
        for cc in sorted(cc_list):
            future = e.submit(download_one, cc)
            to_do.append(future)
            msg = 'Scheduled for {}:{}'
            print(msg.format(cc, future))

        results = []
        for future in futures.as_completed(to_do):
            res = future.result()
            msg = '{} result: {!r}'
            print(msg.format(future, res))
            results.append(res)
    return len(results)


def main(download_many):
    t0 = time.time()
    count = download_many(POP20_CC)
    elapsed = time.time() - t0
    msg = '\n{} flags downloaded in {:.2f}s'
    print(msg.format(count, elapsed))



if __name__ == '__main__':
    # main(download_many)
    # main(download_futures)
    main(download_many_futures)