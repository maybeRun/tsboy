def clip(text, max_len=80):
    end = None

    if len(text) > max_len:
        space_before = text.rfind(' ', 0, max_len)
        if space_before >= 0:
            end = space_before
        else:
            space_after = text.rfind(' ', max_len)
            if space_after >= 0:
                end = space_after
        if end is None:
            end = len(text)
        return text[:end].rstrip()

print(clip.__defaults__)
print(clip.__code__)
print(clip.__code__.co_varnames)


# 这个版本default的默认值需要从后向前扫描
# inspect模块改进
from inspect import signature
sig = signature(clip)
print(sig)
print(str(sig))
for name, param in sig.parameters.items():
    print('param.kind')

# 参数绑定
def tag(name, title, src, cls):
    pass
import inspect
sig = inspect.signature(tag)
my_tag = {"name": "img", "title": 'Sunset Boulevard', 'src': 'sunset.jpg', 'cls': 'framed'}
bound_args = sig.bind(**my_tag)
print(bound_args)


for name, value in bound_args.arguments.items():
    print(name, '=', value)
del my_tag['name']
# bound_args = sig.bind(**my_tag)

# python3 函数注解
def clip(text:str, max_len:'int > 0'=80) -> str:
    end = None

    if len(text) > max_len:
        space_before = text.rfind(' ', 0, max_len)
        if space_before >= 0:
            end = space_before
        else:
            space_after = text.rfind(' ', max_len)
            if space_after >= 0:
                end = space_after
        if end is None:
            end = len(text)
        return text[:end].rstrip()

print(clip.__annotations__)