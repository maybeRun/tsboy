from functools import reduce


def fact(n):
    return reduce(lambda a, b: a * b, range(1, n + 1))


# lambda 不是一种好的实现
# 通过operator来改进.
from functools import reduce
from operator import mul


def fact(n):
    return reduce(mul, range(1, n + 1))


# 更多代替lambda
# itemgetter 代理 lambda field: field[n]
metro_date = [
    ('Tokyo', 'JP', 36.933, (36.689722, 139.691667)),
    ('Delhi NCR', 'IN', 21.933, (28.613889, 77.208889))
]
from operator import itemgetter

for city in sorted(metro_date, key=itemgetter(1)):
    print(city)

cc_name = itemgetter(1, 0)
print(cc_name)
for city in metro_date:
    print(cc_name(city))

# attrgetter
from collections import namedtuple

LatLong = namedtuple('LatLong', 'lat long')
Metropolis = namedtuple('Metropolis', 'name cc pop coord')
metro_areas = [Metropolis(name, cc, pop, LatLong(lat, long)) for name, cc, pop, (lat, long) in metro_date]
print(metro_areas[0])
print(metro_areas[0].coord.lat)

from operator import attrgetter

name_lat = attrgetter('name', 'coord.lat')
for city in sorted(metro_areas, key=attrgetter('coord.lat')):
    print(name_lat(city))
