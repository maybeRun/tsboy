
class StrKeyDict0(dict):

    def __missing__(self, key):
        # 防止后面重新调用__getitem__陷入死循环
        if isinstance(key, str):
            raise KeyError(key)
        return self[str(key)]

    def get(self, key, default=None):
        try:
            return self[key]
        except KeyError:
            return default

    def __contains__(self, item):
        return key in self.keys() or str(key) in self.keys()

# d = StrKeyDict0([('2', 'two'), ('4', 'four')])
# print(d['2'])
# print(d['4'])
# print(d[1])
# print(d.get('2'))
# print(d.get(4))


# 使用collections.UserDict
import collections

class StrKeyDict(collections.UserDict):

    def __missing__(self, key):
        if isinstance(key, str):
            raise KeyError(key)

        def __contains__(self, item):
            return str(item) in self.data.keys()

        def __setitem__(self, key, value):
            self.data[str(key)] = value

d = StrKeyDict([('2', 'two'), ('4', 'four')])
print(d)
print(d['2'])
print(d['4'])
print(d[1])
print(d.get('2'))
print(d.get(4))
print('2' in d)
print(d.data.keys())