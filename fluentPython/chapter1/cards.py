"""通过python内置函数处理数据.
obj[key]的背后就是__getitem__方法
"""

import collections
import random

Card = collections.namedtuple('Card', ['rank', 'suit'])


class FrenchDeck:
    ranks = [str(n) for n in range(2, 11)] + list('JKQA')
    suits = 'spades diamonds clubs hearts'.split()

    def __init__(self):
        self._cards = [Card(rank, suit) for rank in self.ranks for suit in self.suits]

    def __len__(self):
        return len(self._cards)

    def __getitem__(self, position):
        return self._cards[position]


deck = FrenchDeck()
card = random.choice(deck)
print(len(deck))
print(deck[2])

# 迭代
for i in deck:
    print(i)

# 排序
card = Card(rank='2', suit='clubs')
suit_values = dict(spades=3, hearts=2, diamonds=1, clubs=0)
print(len(suit_values))
def spades_high(card):
    rank_value = FrenchDeck.ranks.index(card.rank)
    return rank_value * len(suit_values) + suit_values[card.suit]

print(sorted(deck, key=spades_high))
